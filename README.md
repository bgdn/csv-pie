# The 🍰 made of CSV

`lein figwheel` to start

## Deploying to Heroku

This assumes you have a
[Heroku account](https://signup.heroku.com/dc), have installed the
[Heroku toolbelt](https://toolbelt.heroku.com/), and have done a
`heroku login` before.

``` sh
git init
git add -A
git commit
heroku create
git push heroku master:master
heroku open
```

Created with [Chestnut](http://plexus.github.io/chestnut/) 0.14.0 (66af6f40).
